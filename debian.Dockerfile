FROM debian:stable

RUN apt-get update 
RUN apt-get install gcc -y
RUN apt-get install git wget tmux zsh -y

# node
#RUN curl -sL https://deb.nodesource.com/setup_current.x | bash -
#RUN apt-get install -y nodejs

COPY . /home/dotfiles
# create takashi user as sudoer
# ./install

CMD /bin/zsh
