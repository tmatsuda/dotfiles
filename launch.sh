#!/bin/bash

# install python
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    if [ -f /etc/lsb-release ]; then
	distri_name="ubuntu"
	command -v wget >/dev/null 2>/dev/null || sudo apt-get install wget
    elif [ -f /etc/debian-release ]; then
	distri_name="debian"
	command -v wget >/dev/null 2>/dev/null || sudo apt-get install wget
    elif [ -f /etc/fedora-release ]; then
	distri_name="fedora"
	command -v wget >/dev/null 2>/dev/null || sudo yum -y install wget
    elif [ -f /etc/centos-release ]; then
	distri_name="centos"
	command -v wget >/dev/null 2>/dev/null || sudo yum -y install wget
    fi
elif [[ "$OSTYPE" =~ "darwin"* ]]; then
    distri_name="osx"
    command -v wget >/dev/null 2>/dev/null || brew install wget
fi

#install git, tmux, mc
case "$distri_name" in
    "ubuntu")
	sudo apt-get install git tmux mc emacs
	wget https://repo.continuum.io/archive/Anaconda3-4.3.1-Linux-x86_64.sh
	bash Anaconda3-4.3.1-Linux-x86_64.sh
	;;
    "debian")
	wget https://repo.continuum.io/archive/Anaconda3-4.3.1-Linux-x86_64.sh
	bash Anaconda3-4.3.1-Linux-x86_64.sh
	sudo apt-get install git tmux mc emacs
	;;
    "fedora")
	wget https://repo.continuum.io/archive/Anaconda3-4.3.1-Linux-x86_64.sh
	bash Anaconda3-4.3.1-Linux-x86_64.sh
	sudo yum -y install git tmux mc emacs
	;;
    "centos")
	wget https://repo.continuum.io/archive/Anaconda3-4.3.1-Linux-x86_64.sh
	bash Anaconda3-4.3.1-Linux-x86_64.sh
	sudo yum -y install git tmux mc emacs
	;;
    "osx")
	wget https://repo.continuum.io/archive/Anaconda3-4.4.0-MacOSX-x86_64.sh
	bash Anaconda3-4.4.0-MacOSX-x86_64.sh
	brew install git
	brew install tmux
	brew install mc
	brew install ssh
	brew install fzf
	brew install global
	brew install zplug
	echo "export ZPLUG_HOME=/usr/local/opt/zplug" >> ~/.zshrc
	source ~/.zshrc	
	echo "source $ZPLUG_HOME/init.zsh" >> ~/.zshrc
	source ~/.zshrc
	zplug "b4b4r07/enhancd", use:init.sh
	;;
esac
# install bashrc

# install emacsd

# install the other software

# vim setup
mkdir -p ~/.vim/pack/plugins/start
pushd ~/.vim/pack/plugins/start/
git clone https://github.com/vim-utils/vim-man.git
git clone https://github.com/fatih/vim-go.git
popd
