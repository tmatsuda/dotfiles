#!/bin/bash
ln -s ~/dotfiles/.zshrc ~/.zshrc
ln -s ~/dotfiles/.tmux.conf ~/.tmux.conf
ln -s ~/dotfiles/.bashrc ~/.bashrc
ln -s ~/dotfiles/.zshrc_go ~/.zshrc_go

## xcode-select --install

# homebrew
## /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
echo "brew update" && brew update
echo "brew install wget git tmux mc ssh fzf global zsh pyenv the_silver_searcher gnutils" && brew install wget git tmux mc ssh fzf global zsh pyenv the_silver_searcher gnutils

# zplug
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
echo "export ZPLUG_HOME=/usr/local/opt/zplug" >> ~/.zshrc
echo "source $ZPLUG_HOME/init.zsh" >> ~/.zshrc

# sdkman
curl -s "https://get.sdkman.io" | bash

# java
sdk install java 
sdk install micronaut
sdk install maven
sdk install gradle
sdk install scala
sdk install sbt
sdk install kotlin

# kubernetes
brew install kubectl stern
stern version 1.8.0

# install golang
open https://golang.org/dl/

# vim setup
mkdir -p ~/.vim/pack/plugins/start
pushd ~/.vim/pack/plugins/start/
git clone https://github.com/vim-utils/vim-man.git
git clone https://github.com/fatih/vim-go.git
popd

# install colorscheme
## iterm2
pushd ~/Downloads/
git clone https://github.com/Arc0re/Iceberg-iTerm2.git
popd

## tmux
pushd ~/Downloads/
git clone https://github.com/gkeep/iceberg-dark.git
mv iceberg-dark/.tmux ~/
echo "source-file ~/.tmux/iceberg_minimal.tmux.conf" >> ~/.tmux.conf
popd

## vim
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
