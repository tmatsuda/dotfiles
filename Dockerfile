FROM ubuntu:latest as base
ENV LANG=en_US.UTF-8 
ENV LANGUAGE=en_US.UTF-8
COPY . /root/dotfiles
RUN /root/dotfiles/install

CMD /bin/zsh
