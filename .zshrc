# The following lines werer added by compinstall
zstyle ':completition:*' completer _expand _complete _ignored _correct _approximate
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit

bindkey -e

HISTFILE=~/.zsh_history
HISTSIZE=30000
SAVEHIST=30000

export PS1="%n@%d $"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

case ${OSTYPE} in
    darwin*)
        export CLICOLOR=1
        alias ls='ls -G -F'
        ;;
    linux*)
        alias ls='ls --color'
        ;;
esac

# zplug setting
# require zplug
export ZPLUG_PROTOCOL=HTTPS
export ZPLUG_HOME=/root/.zplug
source ${ZPLUG_HOME}/init.zsh

zplug "b4b4r07/enhancd", use:init.sh
zplug "plugins/git", from:oh-my-zsh
zplug "yous/lime"
zplug "zsh-users/zsh-history-substring-search"
zplug "zsh-users/zsh-syntax-highlighting", defer:2

if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

zplug load

# load local config
#source ~/.zshrc_local

# Golang
#source ~/.zshrc_go

# functions
function watchpstrue() {command watch -b ! "ps auxw | grep -v grep | grep $*" }
function watchpsfalse() {command watch -b "ps auxw | grep -v grep | grep $*" }

# zsh by homebrew
export PATH="/usr/local/opt/ncurses/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/ncurses/lib"
export CPPFLAGS="-I/usr/local/opt/ncurses/include"
export PKG_CONFIG_PATH="/usr/local/opt/ncurses/lib/pkgconfig"


if (which zprof > /dev/null 2>&1) ;then
    zprof
fi

alias tmls='tmux ls'
alias tat='tmux attach -t'


