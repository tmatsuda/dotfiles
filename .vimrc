set number
syntax on
set wrap
set showbreak=+++
set textwidth=100
set showmatch	
set hlsearch
set smartcase
set ignorecase
set incsearch
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=2
" when indenting with '>', use 4 spaces width
set shiftwidth=2
" On pressing tab, insert 4 spaces
set expandtab
set autoindent
set ruler	
set undolevels=1000	
set backspace=indent,eol,start
set relativenumber
autocmd BufRead,BufNewFile *.hql set filetype=sql
autocmd BufRead,BufNewFile *.cql set filetype=sql
set clipboard+=unnamed
set clipboard+=autoselect


" beta
let g:neocomplcache_enable_at_startup = 1

call plug#begin('~/.vim/plugged')
Plug 'tomasiser/vim-code-dark'
Plug 'fatih/vim-go', { 'tag': '*' }
Plug 'prabirshrestha/vim-lsp'
let g:lsp_document_highlight_enabled = 0
Plug 'mattn/vim-lsp-settings'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'mattn/vim-sonictemplate'
call plug#end()


"color
colorscheme codedark

"lsp settings
let g:lsp_diagnostics_echo_cursor = 1

function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    " tag jump
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <Plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <Plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')
    
    " refer to doc to add more commands
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

" automatic format
autocmd BufWritePre <buffer> LspDocumentFormatSync

" manual plugin
runtime! ftplugin/man.vim

