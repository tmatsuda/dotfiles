;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)
;; add melpa to package-archives
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/"))
(when (< emacs-major-version 24)
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages")))
;;(add-to-list 'package-archives
;;	     '("melpa-stable" . "http://stable.melpa.org/packages") t)
;;(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (slime web-mode multiple-cursors ensime material-theme highlight-escape-sequences counsel swiper swiper-helm php-mode))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; show line numbers
(global-linum-mode t)
;; set theme
(load-theme 'material t)
;;java
(require 'meghanada)
(add-hook 'java-mode-hook
          (lambda ()
            ;; meghanada-mode on
            (meghanada-mode t)
            (setq c-basic-offset 2)
            ;; use code format
            (add-hook 'before-save-hook 'meghanada-code-beautify-before-save)))

;; Governing environment: ivy on swiper
(ivy-mode 1)
(setq ivy-user-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")
;; keybinds replaced onto swiper and ivy.
;; replacement for incremental search
(global-set-key (kbd "C-s") 'swiper)
;; replace M-x
(global-set-key (kbd "M-x") 'counsel-M-x)
;;resume function
(global-set-key (kbd "C-c C-r") 'ivy-resume)
;; find function
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
;; a keybinding for git
(global-set-key (kbd "C-c g") 'counsel-git)

;; keybinds for Ctrl-h
(keyboard-translate ?\C-h ?\C-?)


;; gtags-keybind
(add-hook 'c-mode-hook 'counsel-gtags-mode)
(add-hook 'c++-mode-hook 'counsel-gtags-mode)

(with-eval-after-load 'counsel-gtags
  (define-key counsel-gtags-mode-map (kbd "M-t") 'counsel-gtags-find-definition)
  (define-key counsel-gtags-mode-map (kbd "M-r") 'counsel-gtags-find-reference)
  (define-key counsel-gtags-mode-map (kbd "M-s") 'counsel-gtags-find-symbol)
  (define-key counsel-gtags-mode-map (kbd "M-,") 'counsel-gtags-go-backward))

;; 

;; flycheck -- for python 
;; Some conficts with ivy-mode might occur.
(add-hook 'after-init-hook #'global-flycheck-mode)

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)

