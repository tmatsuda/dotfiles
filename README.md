# dotfiles

Takashi Matsuda's dotfiles.  

## usage

Simply call the command below from shell.  
```
./launch.sh
```

## Plan

### terminal: 

iterm2 + tmux + zsh

### Program languages
* Golang
  * prefer using latest release
* Python
  * prefer using latest release
  * version: `> 3.7`
  * Library management: venv + poetry + pip

### zsh

* Load fast by lazy loading.
* Fuzzy search by fzf.

### tmux

### item2

Use "iceberg" for the scheme of color.
