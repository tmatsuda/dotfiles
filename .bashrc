# alias ctags="/usr/local/Cellar/ctags/5.8/bin/ctags"
# alias updatedb="sudo /usr/libexec/locate.updatedb"
# #alias rm="rmtrash"
# alias ractip_hom="~/cbrc/ractip_hom/src/ractip_hom"
# alias academic="open http://academic.research.microsoft.com"
# alias scholar="open http://scholar.google.com"
# alias evd="cd ~/Study/ebola"
# alias ima="pwd"
# alias pipupdate="pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs pip install -U"
# alias ls="gls --color=auto"
# alias filesizeM="du -h | grep -r [0-9][0-9][0-9]M"
# alias filesizeG="du -h | grep -r [0-9]G"
# #alias emacs="/Applications/Emacs.app/Contents/MacOS/Emacs -nw"


# export CLUSTERHOMT="kakacy@gw1.cb.k.u-tokyo.ac.jp:~/cbrc/ractip_hom/ractip_hom/testdata/dataset/Concatenated2/jikkenn/testable"
# export HOME_LOCAL=~/local
# export CENTROID=$HOME_LOCAL/centroid_fold-0.0.9-macosx10.5
# export PGHOME=~/.emacs.d/site-lisp/ProofGeneral-4.2/ProofGeneral
# export PROOFGENERAL=$PGHOME/bin
# #export EMACSPATH=/usr/local/Cellar/emacs/25.1/bin
# export TEXPATH=/usr/texbin

# export RACTIP_HOM=~/cbrc/ractip_hom
# export GTEST_ROOT=~/local/gtest-1.7.0

# export RRTEST=$RACTIP_HOM/testdata/dataset/Concatenated2/jikkenn/testable

# export PATH=/usr/local/bin:$PATH:$HOME_LOCAL:$CENTROID:$PROOFGENERAL:$TEXPATH

# export CPLUS_INCLUDE_PATH=$GTEST_ROOT:$GTEST_ROOT/include
# export OMP_NUM_THREADS=4

export HISTSIZE=100000
export HISTTIMEFORMAT='%Y/%m/%d %H:%M:%S'

# export EDITER='subl -w'

# export GOPATH=~/local/gocode
# export VCFTOOLS=~/local/vcftools
# export PATH=$PATH:$GOPATH/bin:$VCFTOOLS/bin
# export LDFLAGS="-L/usr/local/opt/openssl/lib"
# export CPPFLAGS="-I/usr/local/opt/openssl/include"

# export PRIVATEBIN=~/bin
# export PATH=$PATH:$PRIVATEBIN

# unset PYTHONPATH

# source ~/Dropbox/.bashsync
# alias gohi='cd ~/Study/ebola/H3N2_HIdata/H3N2_integrated_'
# alias trash='open ~/.Trash'

source ~/.bash_completion

#fzf
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
#enhancd
[ -f ~/dotfiles/enhancd/init.sh ] && source ~/dotfiles/enhancd/init.sh

#emacs -nw
alias emacs="emacs -nw"
